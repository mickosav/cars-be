<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{

    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mark');
            $table->string('model');
            $table->integer('year');
            $table->integer('max_speed');
            $table->boolean('is_automatic');
            $table->string('engine');
            $table->integer('number_of_doors');
        });
    }

    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
