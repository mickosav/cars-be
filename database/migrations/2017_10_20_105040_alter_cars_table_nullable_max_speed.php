<?php

use Illuminate\Database\Migrations\Migration;

class AlterCarsTableNullableMaxSpeed extends Migration
{
    //Laravel's schema builder does not support modifying columns other than renaming the column.
    //We will need to run raw queries to do them, like this:
    public function up()
    {
        DB::statement('ALTER TABLE `cars` MODIFY `max_speed` INTEGER NULL;');
    }

    public function down()
    {
        DB::statement('ALTER TABLE `cars` MODIFY `max_speed` INTEGER UNSIGNED NOT NULL;');
    }
}
