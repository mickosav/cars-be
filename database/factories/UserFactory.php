<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Car::class, function (Faker $faker) {
    $engineType = collect([
        'Diesel',
        'Gasoline',
        'Natural Gas'
    ]);
    return [
        'mark' => $faker->text(20),
        'model' => $faker->text(30),
        'year' => $faker->numberBetween(1960, 2017),
        'max_speed' => $faker->numberBetween(20, 300),
        //we are using chanceOfGettingTrue parameter set to 50%
        'is_automatic' => $faker->boolean(50),
        'engine' => $engineType->random(),
        'number_of_doors' => $faker->numberBetween(2, 5)
    ];
});