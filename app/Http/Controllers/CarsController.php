<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\CarRequest;

class CarsController extends Controller
{
    public function index()
    {
        $skip = request()->input('skip', 0);
        $take = request()->input('take', Car::get()->count());

        return Car::skip($skip)->take($take)->get();
    }

    public function store(CarRequest $request)
    {
        return Car::create($request->all());
    }

    public function show($id)
    {
        return Car::findOrFail($id);
    }

    public function update(CarRequest $request, $id)
    {
        $car = Car::findOrFail($id);
        $car->update($request->all());
        return $car;
    }

    public function destroy($id)
    {
        Car::destroy($id);
    }
}
