<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mark' => 'required|min:2',
            'model' => 'required|min:2',
            'year' => 'required',
            'max_speed' => 'nullable|numeric|min:20|max:300',
            'is_automatic' => 'required',
            'engine' => 'required',
            'number_of_doors' => 'required|numeric|min:2|max:5'
        ];
    }
}
